
const root = document.querySelector("#root");
const delBtn = document.querySelector(".del-btn").content;
const button = document.createElement("button");
const usersURL = "https://ajax.test-danit.com/api/json/users";
const postsURL = "https://ajax.test-danit.com/api/json/posts";



class Card {
    constructor(usersURL, postsURL) {
        this.usersURL = usersURL;
        this.postsURL = postsURL;   
    }

    async getPosts() {
        const users = await fetch(this.usersURL).then((response) => {
            return response.json();
        })
        let posts = await fetch(this.postsURL).then((response) => {
            return response.json();
        })

        posts = posts.map(post => {
            post.user = users.find(user => user.id === post.userId)
            return post
        })

        posts.forEach(el => {renderPost(el)});
    }
}

async function deletePost(postId) {
    await fetch(`${postsURL}/${postId}`, {
        method: "DELETE"
    })
}

function renderPost({title, body, user, id}) {
    const postElem = document.createElement("div");
    postElem.classList.add("post-element")
    const divName = document.createElement("div");

    const svg = delBtn.querySelector("svg").cloneNode();
    svg.classList.add("delete")
    const path = delBtn.querySelector("path").cloneNode();
    svg.append(path);

    svg.addEventListener("click", async (event) => {
        await deletePost(id);
        event.target.closest(".post-element").remove();
    })

    const pName = document.createElement("h3");
    pName.textContent = user.name;
    pName.classList.add("author");

    const pEmail = document.createElement("p");
    pEmail.textContent = user.email;
    pEmail.classList.add("email");

    const postTitle = document.createElement("h2");
    postTitle.textContent = title;
    postTitle.classList.add("title");

    const postText = document.createElement("p");
    postText.textContent = body;
    postText.classList.add("text");

    
    divName.append(pName, pEmail, svg);
    postElem.append(divName, postTitle, postText);
    root.append(postElem);
}


const posts = new Card(usersURL, postsURL);
posts.getPosts();


const deleteBtn = document.querySelectorAll(".delete")

    
console.log(deleteBtn);

function delPost() {
    deleteBtn.closest(".post-element")

}